fun main() {
    val tim = Player("Tim")

    val louise = Player("Louise", 5)


    val thelma = Player("Thelma", 4, 8, 34)
    thelma.show()

    val percy = Player("Percy", 2, 5, 1000)
    percy.show()

    val axe = Weapon("Axe", 12)
    thelma.weapon = axe
    println(thelma.weapon.name)
    println(axe.name)

    axe.damageInflicted = 24
    println(axe.damageInflicted)
    println(thelma.weapon.damageInflicted)

    tim.weapon = Weapon("Sword", 10)
    println(tim.weapon.name)

    tim.show()

    louise.weapon = tim.weapon
    louise.show()

    val redPotion = Loot("Red Potion", LootType.POTION, 7.50)
    tim.getLoot(redPotion)
    val chestArmor = Loot("+3 Chest Armor", LootType.ARMOR, 80.00)
    tim.getLoot(chestArmor)
    tim.showInventory()

    tim.getLoot(Loot("Ring of Protection +2", LootType.RING, 40.25))
    tim.getLoot(Loot("Invisibility Potion", LootType.POTION, 35.95))
    tim.showInventory()

    if (tim.dropLoot(redPotion)) {
                tim.showInventory()
            } else {
        println("you don't have a $redPotion.name")
    }

    val bluePotion = Loot("Blue Potion", LootType.POTION, 6.00)
    if (tim.dropLoot(bluePotion)) {
        tim.showInventory()
    } else {
        println("you don't have ${bluePotion.name}")
    }


    }